package com.gamecat.android_tools.Camera2

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.util.Log
import android.view.TextureView
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.gamecat.android_tools.R

class CameraActivity : AppCompatActivity() {
    var qian: CameraCharacteristics? = null
    var hou: CameraCharacteristics? = null
    var qianId: String = ""
    var houId: String = ""
    var mCamera: CameraDevice? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
//获取相机对象
        var cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        //获取相机id列表
        // 遍历所有可用的摄像头 ID，只取出其中的前置和后置摄像头信息。
        val cameraIdList = cameraManager.cameraIdList
        cameraIdList.forEach { cameraId ->
            val cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId)
//            cameraCharacteristics.
//            if (cameraCharacteristics.isHardwareLevelSupported(REQUIRED_SUPPORTED_HARDWARE_LEVEL)) {
            if (cameraCharacteristics[CameraCharacteristics.LENS_FACING] == CameraCharacteristics.LENS_FACING_FRONT) {
                qian = cameraCharacteristics
                qianId = cameraId
                Log.e("TAG", "cameraCharacteristics: qian")
            } else if (cameraCharacteristics[CameraCharacteristics.LENS_FACING] == CameraCharacteristics.LENS_FACING_BACK) {
                Log.e("TAG", "cameraCharacteristics: hou")
                hou = cameraCharacteristics
                houId = cameraId
            }
//            }
        }

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        cameraManager.openCamera(qianId, object : CameraDevice.StateCallback() {
            override fun onOpened(camera: CameraDevice) {
                mCamera = camera
//                mCamera.
            }

            override fun onDisconnected(camqianIdera: CameraDevice) {
            }

            override fun onError(camera: CameraDevice, error: Int) {
//                Toast.makeText()
                camera.close()
            }

        }, null)
    }

    /**
     * 判断相机的 Hardware Level 是否大于等于指定的 Level。
     */
    fun CameraCharacteristics.isHardwareLevelSupported(requiredLevel: Int): Boolean {
        val sortedLevels = intArrayOf(
            CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY,
            CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED,
            CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL,
            CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_3
        )
        val deviceLevel = this[CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL]
        if (requiredLevel == deviceLevel) {
            return true
        }
        for (sortedLevel in sortedLevels) {
            if (requiredLevel == sortedLevel) {
                return true
            } else if (deviceLevel == sortedLevel) {
                return false
            }
        }
        return false
    }

    private inner class PreviewSurfaceTextureListener : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            return false
        }


        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
//            previewSurfaceTexture = surface
        }

    }

}