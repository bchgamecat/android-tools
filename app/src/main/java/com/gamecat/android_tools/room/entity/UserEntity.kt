package com.gamecat.android_tools.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserEntity(
    var userName: String,

    var passWord: String,
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

)
