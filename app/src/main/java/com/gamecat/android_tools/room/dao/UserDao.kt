package com.gamecat.android_tools.room.dao

import androidx.room.*
import com.gamecat.android_tools.room.entity.UserEntity

@Dao
interface UserDao {
    //查询user表中所有数据
    @get:Query("SELECT * FROM UserEntity")
    val all: List<UserEntity?>?

    @Query("SELECT * FROM UserEntity WHERE 'uid' IN (:userIds)")
    fun loadAllByIds(userIds: IntArray?): List<UserEntity?>?

    //    @Query("SELECT * FROM user   LIMIT 1")
    //    void findUser(User user);

    @Query("SELECT * FROM UserEntity WHERE id =  1")
    fun findUser(): UserEntity?

    @Insert
    fun insertAll(users: UserEntity?)

    @Insert()
    fun insertAll(users: List<UserEntity>?)

    @Delete
    fun delete(vararg users: UserEntity?)

    @Delete
    fun delete(users: List<UserEntity>?)

    @Query("DELETE FROM UserEntity")
    fun deleteAllUser()
}