package com.gamecat.android_tools.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gamecat.android_tools.room.dao.UserDao
import com.gamecat.android_tools.room.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao?
}