package com.gamecat.android_tools.room

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.gamecat.android_tools.R
import com.gamecat.android_tools.databinding.ActivityRoomBinding
import com.gamecat.android_tools.room.entity.UserEntity


class RoomActivity : AppCompatActivity() {
    lateinit var inflate : ActivityRoomBinding
    var db: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         inflate = ActivityRoomBinding.inflate(layoutInflater)
        setContentView(inflate.root)
//数据单例  ,,database-tools 表名
        db = Room.databaseBuilder(applicationContext,
            AppDatabase::class.java, "database-tools")
            .allowMainThreadQueries() //允许在主线程中查询
            .build()
        inflate.button.setOnClickListener {
            initData()
        }
        inflate.button2.setOnClickListener {
            initData100()
        }

        inflate.button3.setOnClickListener {
            db?.userDao()?.delete(UserEntity("","",1))
        }
        inflate.button4.setOnClickListener {
            val findUser = db?.userDao()?.findUser()
            db?.userDao()?.delete(findUser)
        }
    }
    fun initData(){
      //添加一个
        db?.userDao()?.insertAll(UserEntity("xx"+System.currentTimeMillis(),"aaa"))
    }

    fun initData100(){
        //添加100个
        val arrayList = ArrayList<UserEntity>()
        for (i in 1..100){
            arrayList.add(UserEntity("xx"+i,"aaa"+i))
        }
        db?.userDao()?.insertAll(arrayList)
    }
}
