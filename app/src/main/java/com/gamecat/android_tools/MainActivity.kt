package com.gamecat.android_tools

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.TypeReference
import com.gamecat.android_tools.Camera2.Camera2Activity
import com.gamecat.android_tools.databinding.ActivityMainBinding
import com.gamecat.android_tools.room.RoomActivity
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions


class MainActivity : BaseActivity<ActivityMainBinding, MyViewModel>() {

    lateinit var activityMainBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(activityMainBinding.root)
        ViewModelProvider(this).get(MyViewModel::class.java)

        initListener()
//        mVm.l.observe(this, object : Observer<String> {
//            override fun onChanged(t: String?) {
//                activityMainBinding.textView.text = t
//            }
//
//        })
    }


    fun initListener() {
        activityMainBinding.button.setOnClickListener {
//            mVm.l.postValue("ccccc")

            val value = object : TypeReference<String?>() {}
            val newInstance = value.type.javaClass.newInstance()
            Log.e("TAG", "initListener: " + newInstance)
//            val intent = Intent(this, Camera2Activity::class.java)
//
//            XXPermissions.with(this) // 申请安装包权限
//                    //.permission(Permission.REQUEST_INSTALL_PACKAGES)
//                    // 申请悬浮窗权限
//                    //.permission(Permission.SYSTEM_ALERT_WINDOW)
//                    // 申请通知栏权限
//                    //.permission(Permission.NOTIFICATION_SERVICE)
//                    // 申请系统设置权限
//                    //.permission(Permission.WRITE_SETTINGS)
//                    // 申请单个权限
//                    .permission(Permission.CAMERA) // 申请多个权限
////                .permission(Permission.WRITE_EXTERNAL_STORAGE)
//                    .request(object : OnPermissionCallback {
//                        override fun onGranted(permissions: List<String>, all: Boolean) {
//                            if (all) {
//                                startActivity(intent)
//                            } else {
//z
//                            }
//                        }
//
//                        override fun onDenied(permissions: List<String>, never: Boolean) {
//                            if (never) {
//
//                            } else {
//                            }
//                        }
//                    })

//            CrashReport.testJavaCrash();

//            DoraemonKit.showToolPanel()
            val intent = Intent(this, RoomActivity::class.java)
            startActivity(intent)
        }

    }

    override fun initView() {

    }
}