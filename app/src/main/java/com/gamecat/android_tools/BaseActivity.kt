package com.gamecat.android_tools

import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.gamecat.android_tools.databinding.ActivityMainBinding
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<V : ViewBinding, VM : ViewModel> : AppCompatActivity() {
    lateinit var mVm: VM
    lateinit var mVivwBind: V
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
//        mVm = ViewModelProvider(this).get(mVm.javaClass)
//        mVivwBind =  ActivityMainBinding.inflate(LayoutInflater.from(this))
        //获得泛型参数的实际类型
//        Class<VM> vmClass = (Class<VM>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        initView()
    }

    inline fun <reified T : Any> new(): T {
        val clz = T::class.java
        val mCreate = clz.getDeclaredConstructor()
        mCreate.isAccessible = true
        return mCreate.newInstance()
    }

    public abstract fun initView();
}