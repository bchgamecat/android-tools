package com.gamecat.android_tools.network

import android.util.Log
import com.google.gson.Gson
import org.json.JSONObject

///**
// * 将得到的String转换成Entity
// */
//class DefaultResponseParse<Entity>(
//    private val callback: ResponseCallback<Entity>,
//    private val entityClass: Class<Entity>
//) : ResponseCallback<String> {
//    private val gson: Gson = Gson()
//
//    override fun onSuccess(response: String?) {
//        Log.i("wxf","onSuccess"+response)
//        var basicEntity = parseResponse(response)
//        if (null != basicEntity) {
//            if (basicEntity.code == BasicResponse.CODE_SUCCESS) {
//                val entity = parseEntity(basicEntity.ret)
//                Log.i("wxf", "entity:" + entity)
//                this.callback.onSuccess(entity)
//            } else {
//                onFailed(RequestException(basicEntity.msg, basicEntity.errorCode.toInt()))
//            }
//        } else {
//            onServerException()
//        }
//    }
//
//    override fun onFailed(ex: RequestException) {
//        this.callback.onFailed(ex)
//    }
//
//    private fun parseResponse(response: String?): BasicStringResponse? {
//        var basicEntity: BasicStringResponse? = null
//        var jsonObject: JSONObject?
//
//        try {
//            jsonObject = JSONObject(response)
//        } catch (e: Exception) {
//            jsonObject = null
//        }
//
//        if (jsonObject != null) {
//            basicEntity = BasicStringResponse()
//            basicEntity.code = jsonObject.optInt("code")
//            basicEntity.msg = jsonObject.optString("msg")
//            basicEntity.ret = jsonObject.optString("ret")
//        }
//
//        return basicEntity
//    }
//
//    private fun onServerException() = this.onFailed(ServerException())
//
//    private fun parseEntity(data: String?): Entity? {
//        var entity: Entity? = null
//        try {
//            if (!TextUtils.isEmpty(data)) {
//                entity = this.gson.fromJson(data, this.entityClass)
//            }
//        } catch (e: Exception) {
//
//        }
//
//        return entity
//    }
//
//}