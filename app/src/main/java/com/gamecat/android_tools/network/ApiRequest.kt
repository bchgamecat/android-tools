package com.gamecat.android_tools.network

import com.trello.rxlifecycle2.LifecycleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

///**
// * 负责与服务器接口通信
// */
//open class ApiRequest(private val progress: HttpObserver.Progress?) {
//    /**
//     * 服务器api响应实体类
//     */
//    protected fun <Entity> request(
//        observable: Observable<Entity>,
//        lifecycleTransformer: LifecycleTransformer<Entity>,
//        callback: ResponseCallback<Entity>
//    ) {
//        observable.compose(lifecycleTransformer)
//            .subscribeOn(Schedulers.io())//线程调度,发射事件的线程
//            .unsubscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())//订阅者接收事件的线程
//            .subscribe(createHttpObserver(callback))//订阅
//
//
//    }
//
//    protected open fun <Entity> createHttpObserver(callback: ResponseCallback<Entity>): HttpObserver<Entity> {
//
//        return DefaultHttpObserver(callback, progress)
//    }
//
//}