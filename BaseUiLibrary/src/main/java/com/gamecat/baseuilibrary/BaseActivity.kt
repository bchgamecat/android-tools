package com.gamecat.baseuilibrary

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import me.yokeyword.fragmentation.SupportActivity

open abstract class BaseActivity() : SupportActivity() {
    val activity = this
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//        if (getOpenImmersionBar() != null) {
//            getOpenImmersionBar()
//        }
        bindIngView()
        initView()
        initListener()
        loadData()

    }

    abstract fun bindIngView()

    abstract fun initView()

    abstract fun loadData()

    abstract fun initListener()

    fun showKeyboard(context: Context?, view: View) {
        // 必须给控件加这个方法，否则无效
        view.requestFocus()
        Log.e("TAG", "showKeyboard: ")
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.RESULT_SHOWN)
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    /**
     * 隐藏软键盘
     *
     * @param context
     * @param view
     */
    fun hideSoftKeyboard(context: Context, view: View?) {
        if (view == null) return
        val inputMethodManager = context.getSystemService(
            INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isActive) inputMethodManager.hideSoftInputFromWindow(
            view.windowToken,
            0
        )
    }
}